const path = require( 'path' );

module.exports = {
    context: __dirname,
    entry: [
        './assets/js/app.js',
        './assets/scss/app.scss'
    ],
    output: {
        path: path.resolve( __dirname, 'public' ),
        filename: 'js/app.js',
    },
    module: {
        rules: [
            {
                test: /\.scss/,
                enforce: "pre",
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: { 
                            outputPath: 'css/', 
                            name: 'app.css'
                        }
                    },
                    'sass-loader',
                    'import-glob-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
        ]
    }
};
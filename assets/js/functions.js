import $ from 'jquery';

/**
 * Change the header color foreach section
 * you scroll to.
 */
$.fn.changeHeaderColour = function changeHeaderColour() {
  $(this).each((i, element) => {
    const elementTop = $(element).offset().top;
    const elementBottom = elementTop + $(element).outerHeight();
    const viewportTop = $(window).scrollTop() + 55;

    if (elementBottom <= viewportTop) {
      $('.header').addClass('header--black');
    } else if (elementTop <= viewportTop) {
      $('.header').removeClass('header--black');
    }
  });
};


/**
 * Hide and show element in an offset range.
 */
$.fn.fadeOutOffset = function fadeOutOffset(offset) {
  const className = $(this).attr('class').split(' ')[0];

  if ($('#wrapper').scrollTop() < offset) {
    $(this).addClass(`${className}--show`);
  } else {
    $(this).removeClass(`${className}--show`);
  }
};

/**
 * Custom Cursor (javascript)
 * @todo: needs to be jQuery
 */
const cursor = document.querySelector('.cursor');
const cursorChild = document.querySelector('.cursor__child');

document.addEventListener('mousemove', (e) => {
  cursor.setAttribute('style', `transform: translate3d(${e.pageX - 20}px, ${e.pageY - 20}px, 0px);`);
  cursorChild.setAttribute('style', `transform: translate3d(${e.pageX + -2}px, ${e.pageY + -2}px, 0px);`);
});

document.addEventListener('click', () => {
  cursor.classList.add('cursor--click');

  setTimeout(() => {
    cursor.classList.remove('cursor--click');
  }, 100);
});

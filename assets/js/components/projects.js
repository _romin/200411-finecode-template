import $ from 'jquery';

const projects = {

  props: {},

  /**
   * On click previous slide.
   */
  prev() {
    $(this.props.prev).on('click', () => {
      let slide = Number($('.progress__current').text());

      $(`.${this.props.slide}`).removeClass(`${this.props.slide}--append`);

      const current = $(`.${this.props.slide}--active`);

      if (current.prev().is(':first-child')) {
        return 0;
      }

      current.prev().addClass(`${this.props.slide}--active`);

      current
        .removeClass(`${this.props.slide}--active`)
        .addClass(`${this.props.slide}--remove`);

      setTimeout(() => {
        current.next()
          .removeClass(`${this.props.slide}--active`)
          .removeClass(`${this.props.slide}--remove`);

        current.removeClass(`${this.props.slide}--remove`);
      }, 500);

      slide -= 1;
      this.setNavigation(slide);

      return true;
    });
  },

  /**
   * On click next slide.
   */
  next() {
    $(this.props.next).on('click', () => {
      let slide = Number($('.progress__current').text());

      $(`.${this.props.slide}`).removeClass(`${this.props.slide}--remove`);

      const current = $(`.${this.props.slide}--active`);

      if (current.is(':last-child')) {
        return 0;
      }

      current.next()
        .addClass(`${this.props.slide}--active`)
        .addClass(`${this.props.slide}--append`);

      // @todo its not appneding, its removing
      // Less than desktop
      if ($(window).width() <= 1024) {
        current
          .removeClass(`${this.props.slide}--active`)
          .removeClass(`${this.props.slide}--append`);
      }

      // More than desktop
      if ($(window).width() >= 1024) {
        setTimeout(() => {
          current
            .removeClass(`${this.props.slide}--active`)
            .removeClass(`${this.props.slide}--append`);
        }, 500);
      }


      slide += 1;
      this.setNavigation(slide);

      return true;
    });
  },

  /**
   * On scroll animate the fist slide.
   *
   * @param {number} offset
   */
  animate(offset) {
    if ($('.slide').hasClass('slide--active')) {
      return false;
    }

    if (!offset) {
      $('.slide')
        .first()
        .addClass('slide--append')
        .addClass('slide--active');
    }

    if (($('.section-projects').outerHeight() + offset) <= ($(this.props.container).scrollTop()
        + $(this.props.container).outerHeight())) {
      $('.slide')
        .first()
        .addClass('slide--append')
        .addClass('slide--active');
    }

    return true;
  },

  /**
   * Set custom navigation numbering.
   *
   * @param {string} number
   */
  setNavigationNumbering(number) {
    if (number < 10) {
      return `0${Number(number)}`;
    }

    return `${Number(number)}`;
  },

  /**
   * Set navigation.
   *
   * @param {number} current
   */
  setNavigation(current) {
    const total = $('.slide').length;
    const singleSlide = 100 / total;
    const number = this.setNavigationNumbering(current);

    $('.progress__current').text(number);
    $('.progress__bar-inner').css({ width: `${singleSlide * current}%` });
    $('.total-slides').text(`0${total}`);
  },

  /**
   * Initialise.
   *
   * @param {object} options
   */
  init(options) {
    this.props = options;

    this.setNavigation(1);
    this.next();
    this.prev();
  },
};

export { projects };

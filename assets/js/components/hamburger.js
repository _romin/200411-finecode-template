const hamburger = {

  props: {},

  /**
   * Initial menu wrapper position.
   */
  menuWrapperPosition() {
    return {
      top: this.props.yPosition,
      right: this.props.xPosition,
    };
  },

  /**
   * Initial menu position.
   */
  menuPosition() {
    return {
      transform: 'translate3d(7px, 7px, 0)',
    };
  },

  /**
   * Initialise.
   *
   * @param {object} options
   */
  init(options) {
    this.props = options;
    this.props.menuWrapper.css(this.menuWrapperPosition());

    const limitX = 30;
    const limitY = 30;

    this.props.menuWrapper.on('mousemove', (event) => {
      let xPos = Math.abs((event.pageX - this.props.menuWrapper.offset().left), limitX);
      let yPos = Math.abs((event.pageY - this.props.menuWrapper.offset().top), limitY);

      // Reposition Top
      if (xPos > 60) xPos = 60;

      // Reposition Left
      if (xPos < 0) xPos = 0;

      // Reposition Right
      if (yPos < 0) yPos = 0;

      // Reposition Bottom
      if (yPos > 60) yPos = 60;


      this.props.menu.css({
        transform: `translate3d(${xPos - 30}px, ${yPos - 30}px, 0)`,
      });
    });

    this.props.menuWrapper.on('mouseleave', () => {
      this.props.menu.css(this.menuPosition());
    });
  },
};

export { hamburger };

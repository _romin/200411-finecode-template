import $ from 'jquery';

import WOW from 'wow.js';
import _ from 'lodash';

import Rellax from 'rellax';
import { hero } from './components/hero';
import { hamburger } from './components/hamburger';
import { projects } from './components/projects';

import './functions';
import 'slick-carousel';

$(() => {
  /**
   * Parallax
   */
  Rellax('.rellax', {
    wrapper: '#wrapper',
    center: true,
  });

  /**
   * Wow.js (Animations)
   */
  const wow = new WOW(
    {
      boxClass: 'animate',
      animateClass: 'animated',
      offset: 100,
      mobile: true,
      live: true,
      scrollContainer: '#wrapper',
      resetAnimation: false,
    },
  );
  wow.init();

  /**
   * Split slides.
   */
  projects.init({
    slide: 'slide',
    next: '.buttons__next',
    prev: '.buttons__prev',
    container: '#wrapper',
  });

  /**
   * Wait 2s and remove splash page.
   */
  setTimeout(() => $('.splash').remove(), 5000);

  /**
   * Hero Animations.
   */
  hero.init({
    heading: 'section-hero__heading',
    overlay: 'section-hero__overlay',
    repeat: true,
    loopWait: 5000,
    headingWait: 2000,
    delayStart: 4000, // delay: 0,
    headings: [
      'Websites.',
      'Wordpress.',
      'Web Design.',
      'Dev Ops.',
      'Let\'s Begin!',
    ],
    overlays: [
      'Websites.',
      'Wordpress.',
      'Design',
      'Dev Ops.',
      'Start Now!',
    ],
  });

  /**
   * Testimonials slider.
   */
  $('.section-testimonials__slides').slick({
    infinite: true,
    autoplay: false,
    autoplaySpeed: 4500,
    dots: false,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 0,
    prevArrow: $('.carousel__prev-testimonials'),
    nextArrow: $('.carousel__next-testimonials'),
  });

  /**
   * Toggle Menu
   */
  $('.hamburger__menu, .menu-navigation__close').on('click', () => {
    $('.hamburger__circle').toggleClass('hamburger__circle--open');

    setTimeout(() => {
      $('.menu-navigation').toggleClass('menu-navigation--show');
    }, 250);
  });
});


$(window).on('load resize', () => {
  /**
   * Reposition hamburger.
   */
  hamburger.init({
    xOffset: 0,
    menu: $('.hamburger__menu'),
    menuWrapper: $('.hamburger__wrapper'),
    yPosition: ($('.header__logo').offset().top - 7),
    xPosition: 0,
  });
});


$('#wrapper').on('scroll DOMMouseScroll mousewheel', () => {
  $('.header__logo').fadeOutOffset(150);
  $('.black-section').changeHeaderColour();

  if ($('.section-projects').length && $(window).width() >= 1024) {
    projects.animate(200);
  } else {
    projects.animate();
  }
});
